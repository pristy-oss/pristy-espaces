import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import { sentryVitePlugin } from "@sentry/vite-plugin";
import path from "path";

export default defineConfig({
  base: "/espaces/",
  build: {
    sourcemap: false,
  },
  plugins: [
    vue(),
    sentryVitePlugin({
      org: "jeci-75336e1de",
      project: "pristy-espaces",
      authToken: process.env.SENTRY_AUTH_TOKEN,
    }),
  ],
  optimizeDeps: {
    force: true,
  },
  resolve: {
    preserveSymlinks: true,
    alias: {
      "@": path.resolve(__dirname, "./src"),
    },
  },
  server: {
    port: 8010,
    proxy: {
      "/alfresco": "http://localhost:8080",
      "/collabora": {
        target: "http://localhost:9980",
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/collabora/, ""),
      },
    },
  },
});
