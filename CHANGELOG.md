# Changelog

1.9.0 / 2025-03-03
==================

- Intégration Matomo
- Fix: zoneAction in collabora page and add reload in pagination in root folder of user home
- Add early acess zone
- Fix: expand copy/move tree at the right folder
- Show 404 when idFolder not in UserHome or when access denied
- Searching in userHome folder
- Add error 404 for userHome
- Add badge and correct breadcrumb for userhome
- Correct favorite ellipsis in menu
- Fix: zoneInfo layout & remove comment & add ellipsis in recent zone
- Use variable for zone info and searchBar breakpoint
- FIX: double scrollbar
- FIX: Filename hidden when small screen on collabora page
- FIX: affinage search zone overflow
- FIX: width of search from input texte
- FIX: css information zone 
- FIX: correct action layout in consultation
- FIX: correct zoneInformation conditions
- FIX: double scrollbar on pdfPreview in consultation
- FIX: searchBar appear in consultation page
- FIX: preview pannel overflow
- Fix: File names and actions stayed truncated after closing preview
- Fix: zone information always opening when refresh
- Fix: favorite name too long
- FIX: icon margin in compact view
- Fix: searchBar becomming full to early
- Add icon in compactView
- Add sidebar in navigation for informationZone
- Fix: layout when splitter is open
- Add loading icon when pdf url is not available
- FIX: background color of assignee zone in create workflow popup
- FIX: copy intern link not working on collabora page
- FIX: visualize older version open collabora and add version tag on pdf older version consultation
- Move zoneInfo button in zone action
- FIX: Create from template not working when alfresco folders are named in french
- Fix pdf page css
- Remove package intersection-observer and file visible.js
- FIX: download version filename & version Panel bug
- Improve ellipsis breadcrumb and datatable header css
- FIX: css width for image-video-txt editor/consultation
- correct condition showing breadcrumb or ellipsis breadcrumb
- Correct css breadcrumb & ellipsis breadcrumb
- Correct badge & breadcrumb for consultation page
- Correct layout & css for breadcrumb and info zone (searchBar, action zone ...)
- Handle responsiveness for actionZone & add ScreenStore
- Use ROW_PER_PAGE_OPTIONS variable for pagination
- Use lib DEFAULT_ELEMENT_PER_PAGE variable for pagination
- Add start workflow action in action zone
- remove vue3-observe-visibility
- remove dsb-norge/vue-keycloak-js
- Use libvue workflow service
- Rename hasMoreThan500 -> hasMoreElements
- Add tasks link in topbar
- FIX: logout on welcome page
- FIX : change selected file when a popup is open
- Return to page one after changing sort order, disable keyboard handler when popup opened
- Correct loadWorkspace and correct moveNode tree
- Correct pagination file list
- Correct fileList sort order + remove selection when changing sort
- Adapt selectAll and change file with keyboard
- Add paginator component and add it in fileList page
- Add from when redirect to forgot password
- Filter site list

1.8.0 / 2024-12-02
==================

- Add createWorkflow popup
- Add forgot password link
- Add copy intern link
- Correct export name + export percentage
- Add forgot password link
- Correct pdfComponent css and default viewport height & width
- Correct css zoneInformation + fileList and pdfComponent
- Change navigationView max width
- Correct breadcrumb and text overflow ellipsis for file name
- Remove company home and Site from path for breadcrumb
- Correct css - vue navigation
- Correct CSS + correct mobile breadcrumb
- Correct searching label
- Correct list files in move and copy popup
- Add file name in collabora page
- Add traduction
- Add new Breadcrumb
- Adapt Navigation view
- Correct package version
- Adapt compact and grid view
- Adapt pdf component layout
- Correct component keyboard handling
- Update package @pristy/pristy-libvue 0.30.3
- Correct sort and filter in searching
- Add sort on searching
- Correct searching
- Include grid/list view and naviagtion view
- Inclue pdf component
- Improve overall responsiveness
- Improve responsiveness SerchingPage
- Correct Select All in datatable
- Correct when  breadcrumb too long
- Fix line break when breadcrumb too long
- allow Collabora page to open view and view_comment action
- open collabora for edit or view
- Adapt css logo for custom css
- Add white color for collabora theme
- Correct personnalize darktheme collabora
- Correct typeError e is null on getRole

1.7.1 / 2024-10-01
==================

- Update package @pristy/pristy-libvue 0.27.4
- FIX max elements set to 1k

1.7.0 / 2024-04-12
==================

- Correct color for darktheme
- Synchronise collabora theme with Pristy theme (Dark)
- Correct vite config using ESM syntax
- Add redirect
- Change topbar logo (transparent)
- Correct add Permissions
- Correct visible condition for moveNode in breacrumb
- Add licence on memberList component
- Add scale
- Add permission popup on folders & files
- Add dropdown on last item of breadcrumb
- Add memberList Component + MemberListPopup
- Add filter input on workspace page
- use ThemeStore
- Add Comment on revert version of a file
- Add scrollTopButton in pdf viewer
- Add scrollTop


**Fixed bugs:**

- #2926 FIX compute canCreate
- Correct bug multiple thumbnails
- Correct joinWorkspace popup is opened twice
- Correct create from model tree
- Fix: Proper DataTable data copy to LocalStorage during page navigation


**Update Dependencies**

- update vite - CVE-2024-23331
- Update libvue 0.27.3
- update pristy-collabora-component
- Update primevue version and update components
- Update packages (vite, vitest, sass prettier, ...)

1.6.0 / 2024-01-17
==================

- Add selected all in page and select all in fileList datatable
- Deleting on consultation redirect
- PERMISSION - Consultation & Selection | Add Envoyer par mail
- Open properties popup after adding aspects in informationZone
- Sentry: send only tracing data to alfresco service
- Add key to breadcrumb for item in searching
- Add canUpload in loadData folderpage
- add error message importing when lecteur
- Add see properties in infoZone if lecteur
- Correct rename action in consultation
- Remove sharelink action of action zone
- Remove display in pdf of a pdf file in action zone
- Correct permissions for open with collabora action
- Correct open in new tab for folder in datatable
- Correct permission for display in pdf action for files selection
- Delete changeview (block) for fileList
- Add open in new tab for folders selection
- Add popup properties everywhere
- Correct permission on selection
- Add ThemeService
- Correct move node
- Correct create file from template
- Correct permissions on workspaces
- Correct results displaying in searching and workspaces list
- #1976 Move keycloak code in libvue
- update dependencies
- Use Nodejs 20.9

1.5.2 / 2023-11-25
==================

- #2684 force pdf extention when downloading pdf file
- #2839 fix date filter problem
- #2838 fix wide breadcrumb
- #2831-#2832 No rendition on folder
- #2837 Adding pagination on FolderPage
- Update on getChildren API

1.5.1 / 2023-11-15
==================

- FIX: Don't use ACS_USERNAME to check if we are auth.
- Correct css for actionZone for mobile

1.5.0 / 2023-11-06
==================

**New Features and Enhancements:**

- Correct favorite node Icon
- Correct condition for login routing
- LoginPage: Correct reconnection at every refresh + redirection when autenticate and on logging page
- Change window.documentTitle for most of the page
- reactivate restore on pdf
- remove webpack reference
- favorite menu, update file icon
- enable sharelink in publicationZone
- Keycloak: load user.person after loggin
- Keycloak: wait for user to be fully login
- FolderPage: change title page of current node
- FolderPage: when drop files, Add progress bar waiting message to auto-delete
- Add popup closing on echap
- LoginPage: route (replace) to target page
- NewWorkspacePopup: add icon
- changeSingleFavorite: menuStore is update in favoriteService
- ZoneActions: on delete force reload
- MoveNodePopup: list current selected node
- CopyNodePopup: list current selected node
- TreeExplorer: auto-expend current node path
- Router: load I18n only on start
- FolderPage: Use navigationStore
- Use new selectionStore to run actions
- ListVersions: Can restore pdf, image and video
- FolderPage: add Spinner and emptyFolder property
- change npm config with node 18
- Primevue Menu with router-link is deprecated
- Move to vue-cli to vite
- support for text/x-web-markdown
- reset selectionStore directly in the router
- ZoneSeachFilter: if facet has a title, use it
- RecherchePage: add ProgressSpinner
- RecherchePage: load filter in one way
- SearchPage: add missing translation
- RecherchePage: addFilterQuery need bucket object
- Update AppTopbar
- Change all css to match css media level 4
- Update pdfjs loading
- ZoneSearchFilter: load SITE and ANCESTOR filter
- RecherchePage: load filter when navigate to page
- SearchPage: rethink the page loading sequence
- Stores moved to pristy-libvue
- ModifyNodePopup: limit width to 45rem
- FileDetailsItem: add foldable panel
- ModifyNodePopup: Add many style to render data
- ModifyNodePopup: add list constraints
- FolderPage: reload data when ZoneInformation change
- FileDetails: add boolean support
- Adding EXIF metadata in demo
- Rewrite ModifyNodePopup to match env-config
- TransalationService: load custom translation file with axios
- ZoneInformation: load aspect list from configuration
- ZoneInformation: Add aspect support
- FolderPage: can edit only when folder
- PdfPage: don't reload all page when modify on metadata
- Change how to open Modify Popup
- ShareLink: force PDF Rendition
- Change default preview url
- Add Sentry
- Clear search URL on searchClear
- Add homeButton breadcrumb
- Correct sort on size and modified-by on datatable
- Create a file from templates
- Create errorPage
- Add action on collabora page

**Fixed bugs:**

- Rooter: fix error getPerson not routing to login page
- Correct metadata editing on folder
- Correct moreResult on searching
- Correct bug on list version of a file
- Hide open in PDF for media files
- Correct download from Datatable
- Correct downloaded file name from version panel
- Correct bug open with MS Office on folder in list view
- Correct selection on delete in actionZone
- Correct bug shareLink popup in shareZone
- #2790: keycloak try renew auth when profile is expired
- SubMenu: fix css
- FolderPage: prevent change this.currentNode two times in different promise
- FolderPage: change on watch() to prevent some error page loading
- Workspace Member page: Prevent fail data are partial
- AppTopbar: fix css search zone
- fix error if data.content is not def
- PageVide: load component only if need
- TranslationService: prevent loading unknow locale
- ModifyNodePopup: load properties if missing
- BreadCrumb: fix route to current folder
- Correcting rule for name
- Fix bug on recent folder
- i18n: silent warn
- Edit metadata pen only if having rights
- Correct url on research from any location after browsing back
- Removing site facet on search removes folder facet
- Remove documentLibrary facet on search from workspace

**Update Dependencies**

- @pristy/pristy-libvue: 0.23.4
- @pristy/pristy-collabora-component: 0.2.0
- Titap: 2.1.11
- @vueuse/core:10.5.0
- axios: 1.5.1
- markdown-it: 13.0.2
- pdfjs-dist: 3.11.174
- pinia: 2.1.6
- primevue: 3.35.0
- remixicon: 3.5.0
- vue-i18n: 9.4.1
- vue-router: 4.2.5

## [1.4.0](https://gitlab.com/pristy-oss/pristy-espaces/-/compare/1.3.1...1.4.0) (2023-07-26)

**New Features and Enhancements:**

- New BreadCrumb Component
- SearchPage: re-think list view
- add actions zone on video, image, pdf and collabora consultation pages
- add confirmation dialog when deleting in ZoneActions
- Metadata in ZoneInformation
- RecherchePage: Redesign 
- Use Remixicon
- FolderPageListe: change filename, title and description css
- New SearchStore and ZoneSearchFilter to draw facets search
- RecherchePage: Add-Remove Chip filter
- Route to PDF page for eml and dwg/dxf files
- Search: Add facet for node TYPE
- Search: Add label for mimetype and node type
- FolderPage: create component RecentData
- Custom OIDC Configuration
- ZoneInformation: Add editing metadata in details panel
- Remove paginator on search page
- Add nbr of search result displayed
- Add version tag on downloaded filename from pdf viewer component
- Move Toast bottom-right
- New icons for workplace visibility
- Improve tiptap editor for Markdown
- LoginPage: Add button linked to ACA

**Fixed bugs:**

- Fix metadata date when property does not exist and modify specific metadata
- Fix user profile missing
- Compare ids when joining workspaces instead of displayName
- Fix hide actions when consulting a version of a file
- Fix confirm dialog opens twice
- Fix validation button to be disabled if cannot move
- Fix expiration date on shareLink to not allow dates before today
- FolderPage: stateKey depends from current node id
- ZoneInfo: display search only on folder page
- Search: exclude TYPE fm:forum
- getRoute: warn if missing param
- Correct topbar searching / Change url on search
- Correct bug listVersion on image and correct downloaded filename
- Translate "New" Button
- #2614: prevent dragenter - rewriting drop file code
- #2535: fix new popup size on mobile
- ModifyNode: Correct tooltip for name rule
- #2538: fix close popup
- #2700: max-width for metadata
- #2698: remove empty "File" category if not a file

**Update Dependencies**

- update pristy-libvue 0.17.2

## [1.3.1](https://gitlab.com/pristy-oss/pristy-espaces/-/compare/1.3.0...1.3.1) (2023-05-22)

**New Features and Enhancements:**

- PdfPage: use getRendition from libvue
- Collabora in correct local
- Update login page

**Fixed bugs:**

- ZoneInformation: bug if node is null
- Use standard CheckBox instead of cb from datatable

**Update Dependencies**

- update pristy-libvue 0.16.5
- update tiptap 2.0.3
- update axios 1.4.0
- update core-js 3.30.2
- update pinia 2.1.2
- update pdfjs 3.6.172
- update primevue 3.29.1
- update remixicon 3.3.0 
- update vue-router 4.2.0

## [1.3.0](https://gitlab.com/pristy-oss/pristy-espaces/-/compare/1.2.1...1.3.0) (2023-05-10)


**New Features and Enhancements:**

- Add download version file and version consultation
- Add details tab on folders and worksapces
- Use stateKey from DataTable
- ZoneInformation : Remove SplitterPanel and use flex design
- Relook image page
- Relook video page
- New Markdown and Html Editor using Tiptap (beta)
- add remixicon
- Add visualize button and remove restore for last version
- Add progress spinner when loading version
- Add send mail popup
- In seacher page, change datatable to dataview with to tempalte list en grid
- Add close icon on zoneInformation
- add lemondap configuration
- load auth config from libvue
- Restore a version file load the new version

**Fixed bugs:**

- Fix window names for videos and image consultation and spelling in french translation
- EditCollabora: hide menu when collabora is open
- Bug getRoute "a is undefined"
- getpath: Sometimes we can't find a root workspace

**Update Dependencies**

- update pristy-libvue 0.16.4
- update caddy 2.6.4
- update saga-green theme

## [1.2.1](https://gitlab.com/pristy-oss/pristy-espaces/-/compare/1.2.0...1.2.1) (2023-03-21)

**New Features and Enhancements:**

- Use COLLABORA_HOSTING_DISCOVERY settings
- More simple button for menu
- Pdf: better print and zoom

**Fixed bugs:**

- Fix Collabora Extensions loading
- changing the way we use services in templates
- Catch Error 404 when login
- Topbar tooltip in bottom

**Update Dependencies**

- update pristy-libvue 0.14.4

## [1.2.0](https://gitlab.com/pristy-oss/pristy-espaces/-/compare/1.1.0...1.2.0) (2023-03-20)

**New Features and Enhancements:**

- PDF: correct computation au loading scale
- Globale cleanning on CSS
- new tab, create with template
- new css popup new document
- sticky information zone
- Change new version popup
- redraw folder presentation
- popup header yellow
- RecherchePage add workspace icone in subtitle
- Add revert button to versions lists
- CollaboraExtensions using fetch from lib in a store
- smaller datatable line in saga-green theme
- Adding document creation from a template
- Add shareLink in action of Datatable
- Add details view on edit with collabora page
- Add details view on image and video pages
- Join automatically a workspace or request to join
- remove window.config use to load configation, use config store instead
- Add favorites to MenuStore
- Add version history display
- Adding a comment when importing a new version
- Change PageVide if we don't have rights to upload in an empty workspace/folder
- Add Share link popup
- Add Keycloak logout
- In member page, differentiating users from groups
- Add action edition in MSOffice
- Display and modify specified metadata (acte model)

**Fixed bugs:**

- Correction loadThumb
- RecherchePage: translate title
- Replace <a> with <router-link> in datatable
- Reduce tree popup size
- Hide Workspace without documentLibrary (workspace.data)
- Fix sort on workspaces in TreeExplorer component
- Bug when loading thumbnail if `e` is undefined
- Removable sort in datatable
- Can't upload if we are in a public or moderated space without permission
- Fix bug opening action menu
- Hide action on file/folder if no writing rights
- Hide multiple actions if no write permission
- Correction drag and drop
- Remove drag and drop if siteConsumer
- Fix search : removal of default date sorting
- On login page Add error message if id or password empty
- In breadcrumb path, display title of workspace instead of id
- Fix to prevent t.path.element is undefined errors
- Correction of folder navigation in search page
- add redirect on errors without status

**Update Dependencies**

- update pdfjs 3.4.120
- update core-js 3.29.1
- update pinia 2.0.33
- update primevue 3.25.0
- update pristy-libvue 0.13.3
- update dsb-norge/vue-keycloak-js
- updaye axios 1.3.4

## [1.1.0](https://gitlab.com/pristy-oss/pristy-espaces/-/compare/1.0.0...1.1.0) (2023-01-31)

**New Features and Enhancements:**

- Add search inco in topbar
- Limitation du nombre d'upload simultanés
- Ajout colonne "Espace" sur la page de recherche
- Ajout du nom de dossier parent
- Translation to english
- Add keycloak-js to permit OIDC authentication
- Add error page when Alfresco is not started
- Add group management functions
- Rework login page
- Use path for breadcrumb
- Breadcrumb service to load breadcrumb using path in PDF, Collabora, Video, Image and Navigation pages

**Fixed bugs:**

- "Open" action on one folder shouldn't appear when vueList is active
- Fix datatable selection bug

**Update Dependencies**

- Use @pristy/pristy-libvue 0.9.0
- Update primevue to 3.22.1
- Update axios to 1.2.2
- Update vue to 3.2.45

## [1.0.0](https://gitlab.com/pristy-oss/pristy-espaces/-/compare/0.10.0...1.0.0) (2022-12-22)

**New Features and Enhancements:**

- Conserver nombre fichiers par page dans datatable
- Conserver le nombre d’éléments par page dans les dossiers
- Ajout action "importer une nouvelle version" et ImportNewVersionPopup
- Afficher la multi-selection et gérer les events
- trim nom de dossier
- Afficher la description d’un dossier / Fichier
- Mapper les actions dans la zone d'actions
- Gestion page de dossier vide
- Activation du Drag and drop
- Hide drag zone in mobile vue
- Icones Pristy pour la liste des fichiers et la recherche
- Traitement de la multi selection de fichiers, pour les fonctions : copier, déplacer, favoris, supprimer
- Change upload limit to 500Mo
- Toolbar sticky pour l'affichage des pdf et image
- Ajout de la vue liste des éléments
- Ajout de la corbeille au menu options
- Remplacer le "+ Nouveau" par deux boutons
- amélioration affichage boites espaces
- Ajouter le titre de la page Collabora

**Fixed bugs:**

- hauteur minimum pour les dossiers
- breadcrumb mal généré au reload
- Correction zone publication + correction css disable
- Correction orthographique de la fonction upload
- Fix erreur de nom lors de la modification d'un fichier
- bug annulation popup
- fermeture popup création dossier
- MobileMenu wont show on collabora page
- rafraichissement de la navigation
- error name on modifyPopup
- no confirm dialog when no changes for folders
- Bug sur l'erreur 409

**Update Dependencies**

- Update @pristy/pristy-libvue 0.6.2
- update primevue to 3.21.0

## [0.10.0](https://gitlab.com/pristy-oss/pristy-espaces/-/compare/0.9.0...0.10.0) (2022-11-23)

**New Features and Enhancements:**

- Update filters search page
- Search for folders

**Fixed bugs:**

- Menu favoris fix loading image, video, pdf, ..


## [0.9.0](https://gitlab.com/pristy-oss/pristy-espaces/-/compare/0.8.0...0.9.0) (2022-11-18)

**New Features and Enhancements:**

- Waiting message during upload
- Favourite menu, reading files
- Add favourite spaces
- Hide new button if player
- Menu items truncated if too long
- Remove space act from space list
- search bar from workspace enabled
- Search on folders

**Update Dependencies**

- Update primevue 3.18.1
- Update @pristy/pristy-libvue 0.4.1

## [0.8.0](https://gitlab.com/pristy-oss/pristy-espaces/-/compare/0.7.0...0.8.0) (2022-11-04)

- Viewing and managing favourites
- Deleting and modifying spaces
- "Flex" display
- Copy / move action on files and folders
- Connecting the search bar
- Display of version numbers
- Update page title and logo
- Adding online editing with Collabora Online

**Update Dependencies**

- Update pristy-libevue 0.3.2

## [0.7.0](https://gitlab.com/pristy-oss/pristy-espaces/-/compare/0.6.0...0.7.0) (2022-10-24)

- Menu simplification
- Correction of too long file display

**Update Dependencies**

- Update pristy-libevue 0.3.1

## [0.6.0](https://gitlab.com/pristy-oss/pristy-espaces/-/compare/0.5.0...0.6.0) (2022-10-12)

- Setting up the Action Zone
- Handling of pdf errors
- Change text on home page
- Login : Display of password error
- Css popup phone/tablet/pc version
- Action Zone mobile version

**Fixed bugs:**

- Favorites menu bug
- Folder creation name field error
- Folder creation, error handling

**Update Dependencies**

- Update pristy-libevue 0.2

## [0.5.0](https://gitlab.com/pristy-oss/pristy-espaces/-/compare/0.4.1...0.5.0) (2022-09-19)

- Adding image display
- Added video display
- Correction of title display
- Added viewing of office documents (pdf format)
- Form creation space, id only
- Force creation of act space in private
- Add breadcrumb in the PDF page
- Redesign of the menu

## [0.4.1](https://gitlab.com/pristy-oss/pristy-espaces/-/compare/0.4.0...0.4.1) (2022-08-31)

- Correction of a routing problem

## [0.4.0](https://gitlab.com/pristy-oss/pristy-espaces/-/compare/0.3.0...0.4.0) (2022-08-30)

- Consultation of the pdfs
- Integration of the lib pristy-libvue
- simple search page

## 0.3.0 (2022-07-13)

- First public version
