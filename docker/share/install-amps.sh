#!/usr/bin/bash -xe

# Ajout des modules
#  - support-tools OOTB
#  - https://gitlab.beezim.fr/jeci/alfresco-js-console
# Suppression du module alfresco-googledrive-share-community

NEXUS_API=https://nexus.jeci.tech/service/rest/v1
NEXUS_PUBLIC=https://nexus.jeci.tech/repository/maven-public
NEXUS_PRISTY=https://nexus.jeci.tech/repository/pristy-releases

env | grep VERSION

CURL="curl -sSL --fail -u $NEXUS_USERNAME:$NEXUS_PASSWORD"

cd "${TOMCAT_HOME}/amps_share/"

rm -f alfresco-googledrive-share-*.amp
if [[ -n "${GOOGLEDOCS_VERSION}" ]]
then
  ARTIFACT_ID=alfresco-googledocs-share
  echo "-- Download ${ARTIFACT_ID} ${GOOGLEDOCS_VERSION}"
  $CURL -O "${NEXUS_PUBLIC}/org/alfresco/integrations/${ARTIFACT_ID}/${GOOGLEDOCS_VERSION}/${ARTIFACT_ID}-${GOOGLEDOCS_VERSION}.amp"
fi

if [[ "${JS_CONSOLE_VERSION}" == *-SNAPSHOT ]]
then
  NEXUS_REPO=pristy-snapshots
  GROUP_ID=de.fmaul
  ARTIFACT_ID=javascript-console-share
  echo "-- Download ${ARTIFACT_ID} ${JS_CONSOLE_VERSION}"
  $CURL -H "accept: application/json" \
        -o "${ARTIFACT_ID}-${JS_CONSOLE_VERSION}.amp" \
        "$NEXUS_API/search/assets/download?sort=version&repository=${NEXUS_REPO}&maven.groupId=${GROUP_ID}&maven.artifactId=${ARTIFACT_ID}&maven.baseVersion=${JS_CONSOLE_VERSION}&maven.extension=amp"
elif [[ -n "${JS_CONSOLE_VERSION}" ]]
then
  ARTIFACT_ID=javascript-console-share
  echo "-- Download ${ARTIFACT_ID} ${JS_CONSOLE_VERSION}"
  $CURL -O "${NEXUS_PRISTY}/de/fmaul/${ARTIFACT_ID}/${JS_CONSOLE_VERSION}/${ARTIFACT_ID}-${JS_CONSOLE_VERSION}.amp"
fi

if [[ -n "${OOTBEE_TOOLS_VERSION}" ]]
then
  ARTIFACT_ID=support-tools-share
  echo "-- Download ${ARTIFACT_ID} ${OOTBEE_TOOLS_VERSION}"
  $CURL -O "${NEXUS_PUBLIC}/org/orderofthebee/support-tools/${ARTIFACT_ID}/${OOTBEE_TOOLS_VERSION}/${ARTIFACT_ID}-${OOTBEE_TOOLS_VERSION}-amp.amp"
fi

find -type f -name "*.amp"

#Installation des modules
export MMT_JAR=$(find ${TOMCAT_HOME}/alfresco-mmt/ -type f -name "*.jar")
find ${TOMCAT_HOME}/webapps/share -type d -print0 | xargs -0 chmod 755
java -jar ${MMT_JAR} install ${TOMCAT_HOME}/amps_share/ ${TOMCAT_HOME}/webapps/share \
  -nobackup -directory -force

find -type f -name "*.amp" -delete
