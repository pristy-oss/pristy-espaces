#!/usr/bin/bash -xe

# Ajout des modules
#  - support-tools OOTB
#  - https://gitlab.beezim.fr/jeci/alfresco-js-console
#  - pristy-core
# Suppression du module alfresco-googledrive-repo-community

NEXUS_API=https://nexus.jeci.tech/service/rest/v1
NEXUS_PUBLIC=https://nexus.jeci.tech/repository/maven-public
NEXUS_PRISTY=https://nexus.jeci.tech/repository/pristy-releases

env | grep VERSION

CURL="curl -sSL --fail -u $NEXUS_USERNAME:$NEXUS_PASSWORD"

cd "${TOMCAT_HOME}/amps/"

rm -f alfresco-googledrive-repo-community-*.amp
if [[ -n "${GOOGLEDOCS_VERSION}" ]]
then
  ARTIFACT_ID=alfresco-googledocs-repo-community
  echo "-- Download ${ARTIFACT_ID} ${GOOGLEDOCS_VERSION}"
  $CURL -O "${NEXUS_PUBLIC}/org/alfresco/integrations/${ARTIFACT_ID}/${GOOGLEDOCS_VERSION}/${ARTIFACT_ID}-${GOOGLEDOCS_VERSION}.amp"
else
  rm -rf /usr/local/tomcat/webapps/alfresco/WEB-INF/classes/alfresco/subsystems/googledocs/
  rm -rf /usr/local/tomcat/webapps/alfresco/WEB-INF/classes/alfresco/module/org.alfresco.integrations.google.docs/
fi

if [[ "${JS_CONSOLE_VERSION}" == *-SNAPSHOT ]]
then
  NEXUS_REPO=pristy-snapshots
  GROUP_ID=de.fmaul
  ARTIFACT_ID=javascript-console-repo
  echo "-- Download ${ARTIFACT_ID} ${JS_CONSOLE_VERSION}"
  $CURL -H "accept: application/json" \
        -o "${ARTIFACT_ID}-${JS_CONSOLE_VERSION}.amp" \
        "$NEXUS_API/search/assets/download?sort=version&repository=${NEXUS_REPO}&maven.groupId=${GROUP_ID}&maven.artifactId=${ARTIFACT_ID}&maven.baseVersion=${JS_CONSOLE_VERSION}&maven.extension=amp"
elif [[ -n "${JS_CONSOLE_VERSION}" ]]
then
  ARTIFACT_ID=javascript-console-repo
  echo "-- Download ${ARTIFACT_ID} ${JS_CONSOLE_VERSION}"
  $CURL -O "${NEXUS_PRISTY}/de/fmaul/${ARTIFACT_ID}/${JS_CONSOLE_VERSION}/${ARTIFACT_ID}-${JS_CONSOLE_VERSION}.amp"
fi

if [[ -n "${OOTBEE_TOOLS_VERSION}" ]]
then
  ARTIFACT_ID=support-tools-repo
  echo "-- Download ${ARTIFACT_ID} ${OOTBEE_TOOLS_VERSION}"
  $CURL -O "${NEXUS_PUBLIC}/org/orderofthebee/support-tools/${ARTIFACT_ID}/${OOTBEE_TOOLS_VERSION}/${ARTIFACT_ID}-${OOTBEE_TOOLS_VERSION}-amp.amp"
fi

if [[ "${AUTH_HMAC_VERSION}" == *-SNAPSHOT ]]
then
  NEXUS_REPO=pristy-snapshots
  GROUP_ID=fr.jeci.alfresco
  ARTIFACT_ID=authentication-hmac-platform
  echo "-- Download ${ARTIFACT_ID} ${AUTH_HMAC_VERSION}"
  $CURL -H "accept: application/json" \
        -o "${ARTIFACT_ID}-${AUTH_HMAC_VERSION}.amp" \
        "$NEXUS_API/search/assets/download?sort=version&repository=${NEXUS_REPO}&maven.groupId=${GROUP_ID}&maven.artifactId=${ARTIFACT_ID}&maven.baseVersion=${AUTH_HMAC_VERSION}&maven.extension=amp"
elif [[ -n "${AUTH_HMAC_VERSION}" ]]
then
  ARTIFACT_ID=authentication-hmac-platform
  echo "-- Download ${ARTIFACT_ID} ${AUTH_HMAC_VERSION}"
  $CURL -O "${NEXUS_PRISTY}/fr/jeci/alfresco/${ARTIFACT_ID}/${AUTH_HMAC_VERSION}/${ARTIFACT_ID}-${AUTH_HMAC_VERSION}.amp"
fi

if [[ "${PRISTY_CORE_VERSION}" == *-SNAPSHOT ]]
then
  NEXUS_REPO=pristy-snapshots
  GROUP_ID=fr.jeci.alfresco
  ARTIFACT_ID=pristy-core-platform
  echo "-- Download ${ARTIFACT_ID} ${PRISTY_CORE_VERSION}"
  $CURL -H "accept: application/json" \
        -o "${ARTIFACT_ID}-${PRISTY_CORE_VERSION}.amp" \
        "$NEXUS_API/search/assets/download?sort=version&repository=${NEXUS_REPO}&maven.groupId=${GROUP_ID}&maven.artifactId=${ARTIFACT_ID}&maven.baseVersion=${PRISTY_CORE_VERSION}&maven.extension=amp"
elif [[ -n "${PRISTY_CORE_VERSION}" ]]
then
  ARTIFACT_ID=pristy-core-platform
  echo "-- Download ${ARTIFACT_ID} ${PRISTY_CORE_VERSION}"
  $CURL -O "${NEXUS_PRISTY}/fr/jeci/alfresco/${ARTIFACT_ID}/${PRISTY_CORE_VERSION}/${ARTIFACT_ID}-${PRISTY_CORE_VERSION}.amp"
fi

find -type f -name "*.amp"

export MMT_JAR=$(find ${TOMCAT_HOME}/alfresco-mmt/  -type f -name "*.jar")
find ${TOMCAT_HOME}/webapps/alfresco -type d -print0 | xargs -0 chmod 755
java -jar ${MMT_JAR} install ${TOMCAT_HOME}/amps/ ${TOMCAT_HOME}/webapps/alfresco -nobackup -directory -force

find -type f -name "*.amp" -delete
