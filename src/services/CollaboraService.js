import { useCollaboraStore, useNavigationStore } from "@pristy/pristy-libvue";

class CollaboraService {
  canOpenWithCollabora(item) {
    if (item.content === undefined) {
      return false;
    }

    const collaboraStore = useCollaboraStore();
    return (
      collaboraStore.checkIfExists(item.content.mimeType, "edit") ||
      collaboraStore.checkIfExists(item.content.mimeType, "view") ||
      collaboraStore.checkIfExists(item.content.mimeType, "view_comment")
    );
  }

  getRoute(item, site) {
    if (item === undefined || item.id === undefined) {
      console.warn("Can't get route for item undefined");
      return {};
    }

    if (item.isFolder || item.content === undefined) {
      return this.getRouteFolder(item, site);
    } else if (
      item.content.mimeType === "image/vnd.dxf" ||
      item.content.mimeType === "image/vnd.dwg" ||
      item.content.mimeType === "message/rfc822"
    ) {
      return { name: "consultation-pdf", params: { id: item.id } };
    } else if (item.content.mimeType.startsWith("image/")) {
      return { name: "consultation-image", params: { id: item.id } };
    } else if (item.content.mimeType.startsWith("video/")) {
      return { name: "consultation-video", params: { id: item.id } };
    } else if (
      item.content.mimeType.startsWith("text/x-markdown") ||
      item.content.mimeType.startsWith("text/x-web-markdown")
    ) {
      return { name: "md-editor", params: { id: item.id } };
    } else if (item.content.mimeType.startsWith("text/html")) {
      return { name: "html-editor", params: { id: item.id } };
    } else if (item.content.mimeType.startsWith("text/plain")) {
      return { name: "text-editor", params: { id: item.id } };
    } else if (item.content.mimeType === "application/pdf") {
      return { name: "consultation-pdf", params: { id: item.id } };
    } else {
      const collaboraStore = useCollaboraStore();
      if (collaboraStore.checkIfExists(item.content.mimeType, "edit")) {
        return {
          name: "edit-collabora",
          params: { action: "edit", id: item.id },
        };
      }
      if (collaboraStore.checkIfExists(item.content.mimeType, "view")) {
        return {
          name: "edit-collabora",
          params: { action: "view", id: item.id },
        };
      }
      if (collaboraStore.checkIfExists(item.content.mimeType, "view_comment")) {
        return {
          name: "edit-collabora",
          params: { action: "view_comment", id: item.id },
        };
      }
    }

    // TODO change default to special page
    return { name: "consultation-pdf", params: { id: item.id } };
  }

  getRouteFolder(item, site) {
    if (site === undefined || site.name === undefined) {
      if (useNavigationStore().isInUserHome) {
        return {
          name: "navigation-user-home-dossier",
          params: {
            IdUser: item.path.elements[2].name,
            FolderId: item.id,
          },
        };
      }
      let workspace = item.path
        ? item.path.elements.find((parent) => parent.nodeType === "st:site")
        : { name: "-" };

      if (workspace === undefined || workspace.name === undefined) {
        console.warn("getRouteFolder can't find workspace", item, site);
        return {};
      }

      return {
        name: "navigation-dossier",
        params: { id: workspace.name, FolderId: item.id },
      };
    } else {
      return {
        name: "navigation-dossier",
        params: { id: site.name, FolderId: item.id },
      };
    }
  }
}

export default new CollaboraService();
