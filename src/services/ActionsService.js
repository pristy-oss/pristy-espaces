/**
 *  Copyright (C) 2022 - Jeci SARL - https://jeci.fr
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see https://www.gnu.org/licenses/agpl-3.0.html.
 */

import axios from "axios";
import {
  alfrescoFileService,
  alfrescoNodeService,
  useConfigStore,
  useNavigationStore,
} from "@pristy/pristy-libvue";
import collaboraService from "@/services/CollaboraService";
import router from "@/router.js";

class ActionsService {
  downloadFile(file) {
    let url = alfrescoFileService.getContentUrl(file.id);
    axios
      .get(url, { responseType: "blob" })
      .then((response) => {
        const blob = new Blob([response.data], {
          type: file.content.mimetype,
        });
        const link = document.createElement("a");
        link.href = URL.createObjectURL(blob);
        link.download = file.name;
        link.click();
        URL.revokeObjectURL(link.href);
      })
      .catch(console.error);
  }

  toggleDownloadFile(nodes) {
    if (!Array.isArray(nodes)) {
      this.downloadFile(nodes);
    } else if (nodes.length > 0) {
      nodes.forEach((file) => {
        this.downloadFile(file);
      });
    }
  }
  getRouteForNavigationInNewTab(workspaceId, item) {
    return {
      name: "navigation-dossier",
      params: {
        id: workspaceId,
        FolderId: item.id,
      },
    };
  }
  getRouteForOpenInNewTab(item) {
    let destinationName = "";
    let routeData;
    if (item.content.mimeType.startsWith("image/")) {
      destinationName = "consultation-image";
    } else if (item.content.mimeType.startsWith("video/")) {
      destinationName = "consultation-video";
    } else if (collaboraService.canOpenWithCollabora(item)) {
      routeData = {
        name: "edit-collabora",
        params: { action: "edit", id: item.id },
      };
      return routeData;
    } else {
      routeData = {
        name: "consultation-pdf",
        params: {
          id: item.id,
        },
      };
      return routeData;
    }
    routeData = {
      name: destinationName,
      params: {
        id: item.id,
      },
    };
    return routeData;
  }

  getItemName(destId, item) {
    return alfrescoNodeService.getChildren(destId).then((data) => {
      let itemName = item.name;
      let destChildren = data.children;
      let initialItemName = item.isFile
        ? this.removeExtension(itemName)
        : itemName;

      let increment = destChildren.filter((child) =>
        child.name.startsWith(initialItemName),
      ).length;
      if (increment > 0) {
        itemName = item.isFile
          ? `${initialItemName}-${increment}.${this.getExtension(itemName)}`
          : `${initialItemName}-${increment}`;
      }
      return itemName;
    });
  }

  getExtension(fileName) {
    let initialFileName = fileName.split(".");
    return initialFileName[initialFileName.length - 1];
  }

  removeExtension(fileName) {
    return fileName.substring(0, fileName.lastIndexOf(".")) || fileName;
  }

  getMimeTypeRoute(node) {
    const mimeTypeRouteMapping = {
      "image/": "image",
      "video/": "video",
      "text/x-markdown": "md",
      "text/x-web-markdown": "md",
      "text/html": "html",
      "text/plain": "text",
      "image/vnd.dxf": "pdf",
      "image/vnd.dwg": "pdf",
      "message/rfc822": "pdf",
      "application/pdf": "pdf",
    };
    const mimeType = node.content.mimeType;
    const matchedRoute = Object.entries(mimeTypeRouteMapping).find(([prefix]) =>
      mimeType.startsWith(prefix),
    )?.[1];

    if (matchedRoute) {
      return matchedRoute;
    }

    if (collaboraService.canOpenWithCollabora(node)) {
      return "edit";
    }

    return "";
  }

  buildLink(config, routeName, nodeId) {
    return `${config.ALFRESCO_HOST}${config.APP_ROOT}${routeName}/${nodeId}`;
  }

  getNodeInternLink(node) {
    const config = useConfigStore();
    const navigationStore = useNavigationStore();
    const currentNodeId = node.id;

    if (node.isFile) {
      const routeName = this.getMimeTypeRoute(node);
      return routeName ? this.buildLink(config, routeName, currentNodeId) : "";
    }

    if (!navigationStore.isInUserHome) {
      const currentSiteId = navigationStore.workspace.id;
      const baseUrl = `${config.ALFRESCO_HOST}${config.APP_ROOT}mes-espaces/${currentSiteId}/`;
      return node.name === "documentLibrary"
        ? baseUrl
        : `${baseUrl}${currentNodeId}`;
    }

    const baseUrl = `${config.ALFRESCO_HOST}${config.APP_ROOT}utilisateur/`;
    const userHome = router.currentRoute._value.params?.IdUser || "";

    if (node.path?.elements?.length > 2) {
      return `${baseUrl}${userHome}/${currentNodeId}`;
    }

    if (node.to?.params) {
      const { IdUser, FolderId } = node.to.params;
      return FolderId
        ? `${baseUrl}${IdUser}/${FolderId}`
        : `${baseUrl}${IdUser}`;
    }

    return `${baseUrl}${userHome}`;
  }

  getCurrentLocationInternLink() {
    const navigationStore = useNavigationStore();
    const { currentNode } = navigationStore;
    return this.getNodeInternLink(currentNode);
  }
}

export default new ActionsService();
