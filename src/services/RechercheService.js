/**
 *  Copyright (C) 2022 - Jeci SARL - https://jeci.fr
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see https://www.gnu.org/licenses/agpl-3.0.html.
 */

import { searchApi } from "@pristy/pristy-libvue";

const EXCLUDED_FILES =
  '-TYPE:"dl:dataList" AND -TYPE:"fm:topic" AND -TYPE:"fm:forum" AND -TYPE:"cm:thumbnail" AND -TYPE:"cm:failedThumbnail" AND -TYPE:"cm:rating" AND -TYPE:"dl:issue" AND -TYPE:"dl:todolist" AND -TYPE:"lnk:link" AND -TYPE:"fm:post" AND -TYPE:"st:site" AND -ASPECT:"st:siteContainer" AND -ASPECT:"sys:hidden" AND -cm:creator:System';
const EXCLUDED_FOLDERS = '-ASPECT:"st:siteContainer"';
const ACTES_FILES = '-TYPE:"am:documentActe" AND -TYPE:"am:annexeConstitutive"';
const ACTES_FOLDERS = '-TYPE:"am:dossierSeance"';

class RechercheService {
  getAllFile() {
    const query = {
      query: {
        language: "afts",
        userQuery: null,
        query: `SITE:"_ALL_SITES_" AND (TYPE:"cm:content" OR TYPE:"cm:folder") AND ${EXCLUDED_FILES} AND ${EXCLUDED_FOLDERS} AND ${ACTES_FILES} AND ${ACTES_FOLDERS}`,
      },
      include: ["aspectNames", "properties", "allowableOperations", "path"],
      paging: {
        maxItems: 100,
        skipCount: 0,
      },
    };

    return searchApi.search(query);
  }
  searchNodeTemplates() {
    const query = {
      query: {
        language: "afts",
        query: `PATH:"//app:company_home/app:dictionary/app:node_templates"`,
      },
      include: ["permissions", "properties", "allowableOperations", "path"],
    };
    return searchApi.search(query);
  }
  searchDataDictionnary() {
    const query = {
      query: {
        language: "afts",
        query: `PATH:"//app:company_home/app:dictionary"`,
      },
      include: ["permissions", "properties", "allowableOperations", "path"],
    };
    return searchApi.search(query);
  }
  searchFolderContentModel() {
    const query = {
      query: {
        language: "afts",
        query: `PATH:"//app:node_templates/cm:content_model"`,
      },
      include: ["permissions", "properties", "allowableOperations", "path"],
    };
    return searchApi.search(query);
  }
}

export default new RechercheService();
