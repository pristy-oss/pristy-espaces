/**
 *  Copyright (C) 2022 - Jeci SARL - https://jeci.fr
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see https://www.gnu.org/licenses/agpl-3.0.html.
 */

import { createRouter, createWebHistory, START_LOCATION } from "vue-router";
import {
  useUserStore,
  useConfigStore,
  useSelectionStore,
  useNavigationStore,
  alfrescoNodeService,
} from "@pristy/pristy-libvue";
import {
  loadLocaleMessages,
  setI18nLanguage,
  i18n,
} from "@/services/TranslationService";
import App from "./App.vue";
import collaboraService from "@/services/CollaboraService";
import { useStorage } from "@vueuse/core";
import { usePaginatorStore } from "@/stores/PaginatoreStore.js";
import { nextTick } from "vue";
import { useUiStateStore } from "@/stores/UiStateStore.js";

const PdfPage = () => import("./pages/PdfPage.vue");
const ImagePage = () => import("./pages/ImagePage.vue");
const FolderPage = () => import("./pages/FolderPage.vue");
const VideoPage = () => import("./pages/VideoPage.vue");
const MarkdownPage = () => import("./pages/MarkdownPage.vue");
const HtmlPage = () => import("./pages/HtmlPage.vue");
const TextPage = () => import("./pages/TextPage.vue");

const mimeTypeRouteMapping = {
  "image/": "consultation-image",
  "video/": "consultation-video",
  "text/x-markdown": "md-editor",
  "text/x-web-markdown": "md-editor",
  "text/html": "text-editor",
  "text/plain": "text-editor",
  "image/vnd.dxf": "consultation-pdf",
  "image/vnd.dwg": "consultation-pdf",
  "message/rfc822": "consultation-pdf",
  "application/pdf": "consultation-pdf",
};

const routes = [
  {
    path: "/",
    name: "app",
    component: App,
    children: [
      {
        path: "",
        name: "welcome",
        meta: {
          requiresAuth: true,
        },
        component: () => import("./pages/StartPage.vue"),
      },
      {
        path: "/mes-espaces",
        name: "mes-espaces",
        meta: {
          requiresAuth: true,
        },
        component: () => import("./pages/WorkspacesPage.vue"),
      },
      {
        path: "/mes-espaces/:id/membres",
        name: "espace-membres",
        meta: {
          requiresAuth: true,
        },
        component: () => import("./pages/WorkspaceMemberPage.vue"),
      },
      {
        path: "/mes-espaces/:id",
        name: "navigation",
        meta: {
          requiresAuth: true,
        },
        component: FolderPage,
      },
      {
        path: "/mes-espaces/:id/:FolderId",
        name: "navigation-dossier",
        meta: {
          requiresAuth: true,
        },
        component: FolderPage,
      },
      {
        path: "/pdf/:id",
        name: "consultation-pdf",
        meta: {
          requiresAuth: true,
        },
        component: PdfPage,
      },
      {
        path: "/pdf/:id/:versionId",
        name: "consultation-pdf-version",
        meta: {
          requiresAuth: true,
        },
        component: PdfPage,
      },
      {
        path: "/image/:id",
        name: "consultation-image",
        meta: {
          requiresAuth: true,
        },
        component: ImagePage,
      },
      {
        path: "/image/:id/:versionId",
        name: "consultation-image-version",
        meta: {
          requiresAuth: true,
        },
        component: ImagePage,
      },
      {
        path: "/video/:id",
        name: "consultation-video",
        meta: {
          requiresAuth: true,
        },
        component: VideoPage,
      },
      {
        path: "/video/:id/:versionId",
        name: "consultation-video-version",
        meta: {
          requiresAuth: true,
        },
        component: VideoPage,
      },
      {
        path: "/md/:id",
        name: "md-editor",
        meta: {
          requiresAuth: true,
        },
        component: MarkdownPage,
      },
      {
        path: "/md/:id",
        name: "md-editor-version",
        meta: {
          requiresAuth: true,
        },
        component: MarkdownPage,
      },
      {
        path: "/html/:id",
        name: "html-editor",
        meta: {
          requiresAuth: true,
        },
        component: HtmlPage,
      },
      {
        path: "/html/:id",
        name: "html-editor-version",
        meta: {
          requiresAuth: true,
        },
        component: HtmlPage,
      },
      {
        path: "/text/:id",
        name: "text-editor",
        meta: {
          requiresAuth: true,
        },
        component: TextPage,
      },
      {
        path: "/text/:id/:versionId",
        name: "text-editor-version",
        meta: {
          requiresAuth: true,
        },
        component: TextPage,
      },
      {
        path: "/:action/:id",
        name: "edit-collabora",
        meta: {
          requiresAuth: true,
        },
        component: () => import("./pages/EditCollaboraPage.vue"),
        props: true,
      },
      {
        path: "/:action/:id/:versionId",
        name: "edit-collabora-version",
        redirect: { name: "consultation-pdf-version" },
      },
      {
        path: "/recherche",
        name: "recherche",
        meta: {
          requiresAuth: true,
        },
        component: () => import("./pages/RecherchePage.vue"),
      },
      {
        path: "/:pathMatch(.*)*",
        name: "not-found-page",
        meta: {
          requiresAuth: true,
        },
        component: () => import("./pages/NotFoundPages.vue"),
      },
      {
        path: "/utilisateur/:IdUser",
        name: "navigation-user-home",
        meta: {
          requiresAuth: true,
        },
        component: () => import("./pages/UserHomePage.vue"),
      },
      {
        path: "/utilisateur/:IdUser/:FolderId",
        name: "navigation-user-home-dossier",
        meta: {
          requiresAuth: true,
        },
        component: () => import("./pages/UserHomePage.vue"),
      },
    ],
  },
  {
    path: "/login",
    name: "login",
    component: () => import("./pages/LoginPage.vue"),
  },
  {
    path: "/error/:id",
    name: "error",
    component: () => import("./pages/ErrorPage.vue"),
    props: true,
  },
];

const router = createRouter({
  history: createWebHistory("/espaces"),
  routes,
});

router.beforeEach(function () {
  window.scrollTo(0, 0);
});

// navigation guards
router.beforeEach(async (to, from) => {
  if (from === START_LOCATION) {
    // Load locale only on first page laoding
    let locale =
      to.params.locale ||
      localStorage?.getItem("user.language") ||
      window?.navigator?.language ||
      "fr";

    // load locale messages
    if (!i18n.global.availableLocales.includes(locale)) {
      await loadLocaleMessages(i18n, locale);
    }

    // set i18n language
    setI18nLanguage(i18n, locale);
  }
});

router.beforeEach(async (to, from) => {
  const config = useConfigStore();
  const user = useUserStore();

  if (to.matched.some((record) => record.meta.requiresAuth)) {
    if (config.AUTH === "keycloak" || config.AUTH === "lemonldap") {
      await user.connectKeycloak();
    } else {
      // Get Person to check if we are authenticate
      await user.getPerson();
      if (!user.isLoggedIn) {
        return { name: "login", query: { target: to.path } };
      }
    }
  }

  // Don't go to login if we are loggedIn
  if (user.isLoggedIn && from.name && to.name === "login") {
    return { name: "welcome" };
  }
});

router.beforeEach(async (to) => {
  await useSelectionStore().resetSelection();
  const navigationStore = useNavigationStore();
  if (to.name.startsWith("navigation")) {
    const elementPerPage = useStorage(
      "elementPerPage",
      useConfigStore().DEFAULT_ELEMENT_PER_PAGE,
    );
    let opts = {
      include: [
        "isFavorite",
        "properties",
        "allowableOperations",
        "permissions",
        "path",
        "isLocked",
      ],
      skipCount: 0,
      maxItems: elementPerPage.value,
    };
    usePaginatorStore().resetPagination();
    if (to.name.startsWith("navigation-user-home")) {
      alfrescoNodeService
        .getHomeFolderFromUser(to.params.IdUser)
        .then(async (data) => {
          let user = data.list.entries[0].entry;
          //homeFolderProvider = userHomesHomeFolderProvider -> userHome présent
          if (
            user.properties["cm:homeFolderProvider"] ===
            "userHomesHomeFolderProvider"
          ) {
            try {
              useNavigationStore().hasNoUserHome = false;
              if (!to.params.FolderId) {
                await navigationStore.loadWithOpts(
                  to.params.id,
                  user.properties["cm:homeFolder"],
                  false,
                  opts,
                );
              } else {
                const isFolderInRightUserHome =
                  await alfrescoNodeService.isFolderInUserHome(
                    to.params.FolderId,
                    user.properties["cm:homeFolder"],
                  );
                if (isFolderInRightUserHome) {
                  await navigationStore.loadWithOpts(
                    to.params.id,
                    to.params.FolderId,
                    false,
                    opts,
                  );
                } else {
                  useNavigationStore().hasNoUserHome = true;
                }
              }
            } catch (e) {
              console.error(e);
              useNavigationStore().hasNoUserHome = true;
            }
          } else {
            useNavigationStore().hasNoUserHome = true;
          }
        });
    } else {
      if (to.name.startsWith("navigation")) {
        await navigationStore.loadWithOpts(
          to.params.id,
          to.params.FolderId,
          false,
          opts,
        );
      }
    }
  }
  if (to.name.startsWith("consultation") || to.name.includes("edit")) {
    navigationStore.isInConsultation = true;
    alfrescoNodeService.getNode(to.params.id).then(async (response) => {
      let parentId = response.path.elements[1].id;
      let userHomeFolder = await alfrescoNodeService.getUserHomeNode();
      let userHomeFolderId = userHomeFolder.list.entries[0].entry.id;

      if (parentId === userHomeFolderId) {
        const elementPerPage = useStorage(
          "elementPerPage",
          useConfigStore().DEFAULT_ELEMENT_PER_PAGE,
        );
        let opts = {
          include: [
            "isFavorite",
            "properties",
            "allowableOperations",
            "permissions",
            "path",
            "isLocked",
          ],
          skipCount: 0,
          maxItems: elementPerPage.value,
        };
        useNavigationStore().isInUserHome = true;
        await navigationStore.loadWithOpts(null, to.params.id, false, opts);
      } else {
        let siteId = response.path.name.split("/")[3];
        await navigationStore.load(siteId, to.params.id, false);
      }
    });
  } else {
    navigationStore.isInConsultation = false;
  }
  useUiStateStore().canShowEarlyAccess = !(
    !to.name.startsWith("consultation") &&
    !to.name.startsWith("navigation") &&
    !to.name.includes("edit")
  );
});
router.beforeEach((to, from, next) => {
  // Match /espaces/88888888-4444-4444-4444-000000000000
  const match = to.fullPath.match(/^\/([a-f\d-]+)$/);
  let redirected = false;

  if (match && match.length > 1) {
    const nodeId = match[1];

    alfrescoNodeService
      .getNode(nodeId)
      .then((response) => {
        if (collaboraService.canOpenWithCollabora(response)) {
          next({
            name: "edit-collabora",
            params: { action: "edit", id: nodeId },
          });
          redirected = true;
        } else if (response.content && response.content.mimeType) {
          const mimeType = response.content.mimeType;
          for (const mimeTypePrefix in mimeTypeRouteMapping) {
            if (mimeType.startsWith(mimeTypePrefix)) {
              next({
                name: mimeTypeRouteMapping[mimeTypePrefix],
                params: { id: nodeId },
              });
              redirected = true;
              return;
            }
          }
        } else {
          if (response.isFolder) {
            if (useNavigationStore().isInUserHome) {
              next({
                name: "navigation-user-home",
                params: {
                  IdUser: useUserStore().person.id,
                  FolderId: nodeId,
                },
              });
              redirected = true;
              return;
            } else {
              let siteName = response.path.elements[2].name;
              next({
                name: "navigation-dossier",
                params: { id: siteName, FolderId: nodeId },
              });
              redirected = true;
              return;
            }
          }
        }
        if (!redirected) {
          next();
        }
      })
      .catch((error) => {
        console.error("Erreur lors de la récupération du nœud:", error);
        next(false);
      });
  } else {
    next();
  }
});
router.beforeEach((to, from, next) => {
  // Match /espaces/site/.*
  const match = to.fullPath.match(/^\/site\/(.*)$/);
  if (match && match.length > 1) {
    const nodeId = match[1];
    next({
      name: "navigation",
      params: { id: nodeId },
    });
  } else {
    next();
  }
});
router.beforeEach((to) => {
  let pageTitle = "";
  const nav = useNavigationStore();
  switch (to.name) {
    case "welcome":
      pageTitle = "";
      break;
    case "mes-espaces":
      pageTitle = `${i18n.global.t("window.workspacePageTitle")}`;
      break;
    case "espace-membres":
      pageTitle = `${i18n.global.t("window.workspaceMemberPageTitle")}`;
      break;
    case "navigation":
      pageTitle = nav.workspace.title;
      break;
    case "navigation-dossier":
      pageTitle = nav.currentNode.name;
      break;
    case "recherche":
      pageTitle = `${i18n.global.t("menu.recherche")}`;
      break;
    case "not-found-page":
      pageTitle = "Page Not Found";
      break;
    case "login":
      pageTitle = `${i18n.global.t("login.connexion")}`;
      break;
    case "error":
      pageTitle = "Error";
      break;
    default:
      pageTitle = "";
  }
  const config = useConfigStore();
  window.document.title = `${pageTitle} | ${config.APP_ROOT_TITLE}`;
});

export default router;
