/*
  Copyright (C) 2022 - Jeci SARL - https://jeci.fr

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see https://www.gnu.org/licenses/agpl-3.0.html.
 */

import { defineStore } from "pinia";
import { useNavigationStore } from "@pristy/pristy-libvue";

export const useUiStateStore = defineStore({
  id: "UiStateStore",
  state: () => ({
    isKeyboardNavEnable: false,
    isPreviewPanelVisible: false,
    isPopupOpen: false,
    isSplitterDisplayed:
      JSON.parse(localStorage.getItem("splitter.display")) || false,
    isStaticMenuDisplayed: false,
    sidebarBreakPointWithMenu: 1570,
    sidebarBreakPointWithoutMenu: 1260,
    windowBreakPointWithoutMenuWhenPreviewActive: 1570,
    windowBreakPointWithMenuWhenPreviewActive: 1850,
    actionZoneBreakPointWithMenu: 1650,
    actionZoneBreakPointWithoutMenu: 1430,
    searchBarBreakPoint: 1280,
    affinageZoneBreakPointWithMenu: 1630,
    affinageZoneBreakPointWithoutMenu: 1328,
    zoneInfoBreakPoint: 1430,
    canShowEarlyAccess: false,
  }),
  actions: {
    changeSplitterDisplay() {
      this.isSplitterDisplayed = !this.isSplitterDisplayed;
      localStorage.setItem("splitter.display", this.isSplitterDisplayed);
    },
  },
});
