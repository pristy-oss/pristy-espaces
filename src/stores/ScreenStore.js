import { defineStore } from "pinia";

export const useScreenStore = defineStore("ScreenStore", {
  state: () => ({
    screenWidth: window.innerWidth,
    screenHeight: window.innerHeight,
  }),
  actions: {
    updateScreenSize() {
      this.screenWidth = window.innerWidth;
      this.screenHeight = window.innerHeight;
    },
  },
  getters: {
    isVerySmallScreen: (state) => state.screenWidth <= 767,
    isSmallScreen: (state) => state.screenWidth <= 992,
    isPortrait: (state) => state.screenHeight > state.screenWidth,
  },
});
