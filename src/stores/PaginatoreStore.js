import { defineStore } from "pinia";

export const usePaginatorStore = defineStore("PaginatorStore", {
  state: () => ({
    first: 0,
  }),
  actions: {
    resetPagination() {
      this.first = 0;
    },
  },
});
