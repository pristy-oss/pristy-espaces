# Pristy Espaces

Interface de gestion des espaces de travail.

Workspace management interface.

Users will be able to :

* list the public and moderated workspaces as well as the spaces they are members of
* request to join a moderated workspace

For sites where the user is the manager it will be possible to:

* modify the title, description and content of the site
* change the title, description and visibility
* manage the members of the workspace:
	* invite new users
	* delete members
	* change member permissions


## Installation

1. Install all dependencies

```
npm install
```

2. Compiles project for development

```
npm run serve
```

3. Compiles and minifies for production

```
npm run build
```

4. Lints and fixes files

```
npm run lint
```

## Test

You can use pristy-demo to fully test this application. First start the demo stack, then start the development server.

``` bash
git clone pristy-demo
cd pristy-demo
./start.sh

cd ../pristy-espaces
npm install
npm run serve
```

same with docker image

``` bash
docker run --rm -it --init --name=pristy-espaces -v "$PWD:/usr/src/app" --net host --workdir=/usr/src/app docker.io/library/node:16.15.0 npm run serve
```

Note: Alfresco must be available on http://localhost:8080/alfresco to work.
Also `vue.config.js` and `public/env-config.json` must match.

> As some modules are present on our nexus, you need to add your credentials to your `.envrc` file

## Config

### METADATA

This attribute is a table made to configure metadata fields on Pristy. So, you can define as many fields with the following properties. 

| property    | status    | description                                                                       | options                                                                                   |
|-------------|-----------|-----------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------|
| id          | mandatory | The metadata id                                                                   |                                                                                           |
| labelId     | mandatory | The label id that contains the translated text to display before field            |                                                                                           |
| display     | optional  | List to specify in which composant to display the property, everywhere by default | datatable, zone, popup                                                                    |
| input       | optional  | Correspond to html type of field, text by default                                 | date, datetime, time, email, file, hidden, number, radio, checkbox, range, tel, text, url |
| readOnly    | optional  | Property is editable, if not present is set to false by default                   | true, false                                                                               | 
| modelAspect | optional  | Metadata id aspect (permit to group fields by category)                           |                                                                                           |
| modelType   | optional  | Metadata id type on which to apply display                                        |                                                                                           |
| order       | optional  | Sort by ascending order, has last by default                                      |                                                                                           |

## Licensing

Copyright (C) 2022 - Jeci SARL - https://jeci.fr

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see https://www.gnu.org/licenses/agpl-3.0.html.
